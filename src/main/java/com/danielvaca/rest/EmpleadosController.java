package com.danielvaca.rest;

import com.danielvaca.rest.empleados.Capacitacion;
import com.danielvaca.rest.empleados.Empleado;
import com.danielvaca.rest.empleados.Empleados;
import com.danielvaca.rest.repositorios.EmpleadoDAO;
import com.danielvaca.rest.utils.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import java.net.URI;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/empleados")
public class EmpleadosController {

    @Autowired
    private EmpleadoDAO empDao;

    @GetMapping(path = "/")
    public Empleados getEmpleados() {
        return empDao.getAllEmpleados();
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<Empleado> getEmpleado(@PathVariable() int id) {
        Empleado emp = empDao.getEmpleado(id);
        if(emp == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok().body(emp);
        }
    }
    @PostMapping("/")
    public ResponseEntity<Object> addEmpleado(@RequestBody Empleado emp) {
        Integer id = empDao.getAllEmpleados().getListaEmpleados().size();
        emp.setId(id);
        empDao.addEmpleado.accept(emp);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(emp.getId())
                .toUri();
        return ResponseEntity.created(location).build();
    }
    @PutMapping(value = "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> updEmpleado(@RequestBody Empleado emp) {
        empDao.updEmpleado(emp);
        return ResponseEntity.ok().build();
    }
    @PutMapping(value = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> updEmpleado(@PathVariable int id, @RequestBody Empleado emp) {
        empDao.updEmpleado(id , emp);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Object> delEmpleadoXId(@PathVariable int id){
        String resp = empDao.deleteEmpleado(id);
        if(resp.equals("OK")) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PatchMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> softupdEmpleado(@PathVariable int id, @RequestBody Map<String, Object> updates) {
        empDao.softupdEmpleado(id, updates);
        return ResponseEntity.ok().build();
    }

    @GetMapping(path = "/{id}/capacitaciones")
    public ResponseEntity<List<Capacitacion>> getCapacitacionesEmpleado(@PathVariable int id){
        if (!empDao.getCapacitacionesEmpleado(id).isEmpty()){
            return ResponseEntity.ok().body(empDao.getCapacitacionesEmpleado(id));
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping(path = "/{id}/capacitaciones", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> addCapacitacionEmpleado(@PathVariable int id, @RequestBody Capacitacion cap) {
        if (empDao.addCapacitacion(id, cap)) {
            return ResponseEntity.ok().build();
        }
        return  ResponseEntity.notFound().build();
    }

    @Value("${app.titulo}")
    private String titulo;

    @GetMapping("/titulo")
    public String getAppTitulo() {
        String modo = configuration.getModo();
        return String.format("%s (%s)", titulo, modo);
    }

    @Autowired
    private Environment env;

    @GetMapping("/autor")
    public String getAppAutor() {
        return configuration.getAutor();
        //return env.getProperty("app.author");
    }

    @Autowired
    Configuration configuration;


}