package com.danielvaca.rest.empleados;

public class Capacitacion {
    private String fecha;
    private String titulo;

    public Capacitacion(String fecha, String titulo) {
        this.fecha = fecha;
        this.titulo = titulo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    @Override
    public String toString() {
        return "Capacitacion{" +
                "fecha='" + fecha + '\'' +
                ", titulo='" + titulo + '\'' +
                '}';
    }
}
