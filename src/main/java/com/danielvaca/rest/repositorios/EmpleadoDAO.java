package com.danielvaca.rest.repositorios;

import com.danielvaca.rest.empleados.Capacitacion;
import com.danielvaca.rest.empleados.Empleado;
import com.danielvaca.rest.empleados.Empleados;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

@Repository
public class EmpleadoDAO {
    Logger logger = LoggerFactory.getLogger(EmpleadoDAO.class);
    private static Empleados list = new Empleados();
    static {
        Capacitacion cap1 = new Capacitacion("2020/01/01", "DBA");
        Capacitacion cap2 = new Capacitacion("2020/01/02", "Back End");
        Capacitacion cap3 = new Capacitacion("2020/01/03", "Front End");
        ArrayList<Capacitacion> una = new ArrayList<>();
        una.add(cap1);
        ArrayList<Capacitacion> dos = new ArrayList<>();
        dos.add(cap1);
        dos.add(cap2);
        ArrayList<Capacitacion> todas = new ArrayList<>();
        todas.add(cap3);
        todas.addAll(dos);

        list.getListaEmpleados().add(new Empleado(0, "Antonio", "Lopez", "antonio@hotmail.com", una));
        list.getListaEmpleados().add(new Empleado(1, "Juancho", "Perez", "antonio@hotmail.com", dos));
        list.getListaEmpleados().add(new Empleado(2, "Alma", "Lopez", "antonio@hotmail.com", todas));
    }
    public Empleados getAllEmpleados() {
        logger.debug("Empleados devueltos");
        return list;
    }
    public Empleado getEmpleado(int id) {
        for(Empleado emp: list.getListaEmpleados()) {
            if (emp.getId() == id) {
                return emp;
            }
        }
        return null;
    }

    public Consumer<Empleado> addEmpleado = emp -> list.getListaEmpleados().add(emp);

    public void updEmpleado(Empleado emp) {
        Empleado current = getEmpleado(emp.getId());
        current.setNombre(emp.getNombre());
        current.setApellido(emp.getApellido());
        current.setEmail(emp.getEmail());
    }
    public void updEmpleado(int id, Empleado emp) {
        Empleado current = getEmpleado(id);
        current.setNombre(emp.getNombre());
        current.setApellido(emp.getApellido());
        current.setEmail(emp.getEmail());
    }
    public String deleteEmpleado(int id) {
        Empleado current = getEmpleado(id);
        if (current == null) return "NO";
        Iterator it = list.getListaEmpleados().iterator();
        while( it.hasNext() ) {
            Empleado emp = (Empleado) it.next();
            if (emp.getId() == id) {
                it.remove();
                break;
            }
        }
        return "OK";
    }
    public void softupdEmpleado(int id, Map<String, Object> updates) {
        Empleado current = getEmpleado(id);
        for (Map.Entry<String, Object> update : updates.entrySet()) {
            switch (update.getKey()) {
                case  "nombre":
                    current.setNombre(update.getValue().toString());
                    break;
                case "apellido":
                    current.setApellido(update.getValue().toString());
                    break;
                case "email":
                    current.setEmail(update.getValue().toString());
                    break;
            }
        }
    }
    public List<Capacitacion> getCapacitacionesEmpleado(int id) {
        Empleado current = getEmpleado(id);
        ArrayList<Capacitacion> caps = new ArrayList<Capacitacion>();
        if (current != null) caps = current.getCapacitaciones();
        return caps;
    }

    public Boolean addCapacitacion(int id, Capacitacion cap) {
        Empleado current = getEmpleado(id);
        if(current == null) return false;
        current.getCapacitaciones().add(cap);
        return true;
    }
}
