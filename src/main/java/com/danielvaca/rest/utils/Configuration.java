package com.danielvaca.rest.utils;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("app")
public class Configuration {
    private String titulo;
    private String autor;
    private String modo;

    public Configuration() {

    }

    public Configuration(String titulo, String autor, String modo) {
        this.titulo = titulo;
        this.autor = autor;
        this.modo = modo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getModo() {
        return modo;
    }

    public void setModo(String modo) {
        this.modo = modo;
    }
}
